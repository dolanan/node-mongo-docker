const Server = require("http").Server;
const db = require("monk")("db:27017/data");

const collection = db.get("document");
collection
  .insert([{ a: 1 }, { a: 2 }, { a: 3 }])
  .then(() => {
    console.log("inserted");
  })
  .catch(err => {
    console.log(err);
  });

const server = new Server(async (req, res) => {
    const body = await collection.find();
    res.end(JSON.stringify(body));
});

server.listen(8000);
